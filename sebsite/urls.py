from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^chat/', include('tables.urls')),
    url(r'^', include('users.urls')),


]
