from django.contrib import admin
from .models import Room, Room_Population, Private_Message, Private_Room
# Register your models here.

admin.site.register(Room)
admin.site.register(Room_Population)
admin.site.register(Private_Room)
admin.site.register(Private_Message)
