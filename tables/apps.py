from django.apps import AppConfig


class TablesConfig(AppConfig):
    name = 'tables'

    def ready(self):
        import tables.signals
