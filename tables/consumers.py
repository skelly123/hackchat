# chat/consumers.py
from channels.generic.websocket import AsyncWebsocketConsumer
import json
import time
from .models import Room, Room_Population, Private_Message, Private_Room
from django.contrib.auth.models import User
from django.db.models import F
from hashlib import md5
# from channels import Group


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        print("connecting chatter")
        # create room and make new population
        room, created = Room.objects.get_or_create(title=self.room_name)
        if created:
            Room_Population.objects.create(room=room, population=1)
            pop = 1
        else:
            room_pop = Room_Population.objects.filter(room=room).latest('time')
            pop = room_pop.population + 1
            Room_Population.objects.create(room=room, population=pop)



        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

        # send new Population
        # Send message to WebSocket
        await self.channel_layer.group_send(
            self.room_group_name,
            {
            'type': 'pop_message',
            'population': pop,
            }
        )

        await self.channel_layer.group_send(
            'chat_room_dash',
            {
                'type': 'send_message',
                'update': pop,
            }
        )



    async def disconnect(self, close_code):
        room = Room.objects.get(title=self.room_name)
        room_pop = Room_Population.objects.filter(room=room).latest('time')

        pop = room_pop.population - 1
        if room_pop.population == 1:
            if not room.permanent:
                room.delete()
        else:
            Room_Population.objects.create(room=room, population=pop)
            # room.save()

        # send new Population
        # Send message to WebSocket
        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
            'type': 'pop_message',
            'population': pop,
            }
        )


        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )



    # Receive message from WebSocket
    async def receive(self, text_data):
        print("recieved messsage")
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        message2 = text_data_json['message2']
        logo = text_data_json['logo']
        n = text_data_json['n']
        e = text_data_json['e']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'message2': message2,
                'logo': logo,
                'n': n,
                'e': e,
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        print("chat messsage")
        message = event['message']
        message2 = event['message2']
        logo = event['logo']
        n = event['n']
        e = event['e']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'type': 'chat_message',
            'content': 'chat_message',
            'message': message,
            'message2': message2,
            'logo': logo,
            'time': time.strftime("%H:%M:%S"),
            'n': n,
            'e': e,
        }))

    # change in room group population
    async def pop_message(self, event):
        content = event['type']
        population = event['population']


        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'content': 'pop_message',
            'population': population,

        }))


class RoomConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.group_name = 'chat_room_dash'
        print("joined dash room")
        # Join room group
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        print("left dash room")
        pass

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        print(text_data_json, "this came through ####")

        # Send message to room group
        await self.channel_layer.group_send(
            self.group_name,
            {
                'type': 'send_message',
                'response': 500,
            }
        )

    async def send_message(self, text_data=None):
        print(text_data)
        label_data_list = []
        room_pop_time = []
        for room in Room.objects.all():
            try:
                population = Room_Population.objects.filter(room=room).latest('time').population
            except:
                population = 0
            d = {}
            d["label"]=room.title
            d["data"]= population
            label_data_list.append(d)

            d2 = {}
            d2["room"]=room.title
            room_pop_data = Room_Population.objects.filter(room=room).order_by('time')
            d2["data"] = [{"x":item.time.timestamp(), "y":item.population} for item in room_pop_data]
            room_pop_time.append(d2)

        label_data_list = sorted(label_data_list, key=lambda k: k['data'], reverse=True)
        labels = [i['label'] for i in label_data_list]
        data = [i['data'] for i in label_data_list]
        print(room_pop_time)


        await self.send(text_data=json.dumps(
        {
            'labels': labels,
            'data': data,
            'time_series':room_pop_time,

        }))

class InboxConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']
        # self.user
        self.user = str(self.scope["user"])

        print(self.user)

        # Join room group
        await self.channel_layer.group_add(
            self.user,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        self.user = str(self.scope["user"])
        # Leave room group
        await self.channel_layer.group_discard(
            self.user,
            self.channel_name,
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        print("recieved messsage")
        text_data_json = json.loads(text_data)

        content = text_data_json['content']
        if content == 'send_message':
            from_a = text_data_json['from_a']
            to_b = str(text_data_json['to_b'])
            message = text_data_json['message']
            # time_now = time.strftime("%H:%M:%S")

            #save
            a = User.objects.get(username=from_a)
            b = User.objects.get(username=to_b)
            pm = Private_Message.objects.create(from_a=a, to_b=b, message=message)
            time_now = pm.time
            id = pm.id

            print(to_b)

            # Send message to room group
            await self.channel_layer.group_send(
                to_b,
                {
                    'type': 'inbox_message',
                    'from_a': from_a,
                    'message': message,
                    'time': str(time_now),
                    'id': pm.id,
                }
            )

        if content == 'read_message':
            id = text_data_json['id']
            print("########     ", id, "     ########")
            pm = Private_Message.objects.get(id=id)
            pm.read = True
            pm.save()
            print("marked read")

        if content == 'create_private_room':
            # id = text_data_json['id']
            from_a = text_data_json['from_a']
            to_b = str(text_data_json['to_b'])
            a = User.objects.get(username=from_a)
            b = User.objects.get(username=to_b)
            print("This is a and b ", a, b)
            # while True:
            try:
                pr = Private_Room.objects.create(user_one=a, user_two=b)
                link = pr.link
                print("marked read")

                # Send message to room group
                await self.channel_layer.group_send(
                    to_b,
                    {
                        'type': 'private_chat_request',
                        'from_a': from_a,
                        'link': str(pr.link),
                        'time': str(pr.time.strftime("%H:%M:%S")),
                    }
                )
            except Exception as e:
                print("room already taken/created - using old one")
                # Send message to room group
                pr = Private_Room.objects.get(user_one=a, user_two=b)
                link = pr.link
                await self.channel_layer.group_send(
                    to_b,
                    {
                        'type': 'private_chat_request',
                        'from_a': from_a,
                        'link': str(pr.link),
                        'time': str(pr.time.strftime("%H:%M:%S")),
                    }
                )

        if content == "visted_private_room":
            from_a = text_data_json['from_a']
            to_b = text_data_json['to_b']
            link = text_data_json['link']
            time_val = str(time.strftime("%H:%M:%S"))

            # Send message to room group
            await self.channel_layer.group_send(
                to_b,
                {
                    'type': 'visited_private_room',
                    'from_a': from_a,
                    'link': link,
                    'time': time_val,
                }
            )




    # Receive message from room group
    async def inbox_message(self, event):
        print("chat messsage")
        from_a = event['from_a']
        message = event['message']
        time = event['time']
        id = event['id']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'content': 'inbox_message',
            'from_a': from_a,
            'message': message,
            'time': time,
            'id': id,
        }))

    # Receive message from room group
    async def private_chat_request(self, event):
        print("chat messsage")
        from_a = event['from_a']
        link = event['link']
        time = event['time']


        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'content': 'private_chat_request',
            'from_a': from_a,
            'link': link,
            'time': time,
        }))

    # Receive message from room group
    async def visited_private_room(self, event):
        from_a = event['from_a']
        link = event['link']
        time = event['time']


        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'content': 'visited_private_room',
            'from_a': from_a,
            'link': link,
            'time': time,
        }))




class PrivateChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        print("connecting private chatter")

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()


    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )


    # Receive message from WebSocket
    async def receive(self, text_data):
        print("recieved messsage")
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        message2 = text_data_json['message2']
        logo = text_data_json['logo']
        n = text_data_json['n']
        e = text_data_json['e']
        encrypt = int(text_data_json['encrypt'])

        if encrpyt:
            # Send message to room group
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message,
                    'message2': message2,
                    'logo': logo,
                    'n': n,
                    'e': e,
                }
            )


    # Receive message from room group
    async def chat_message(self, event):
        print("chat messsage")
        message = event['message']
        message2 = event['message2']
        logo = event['logo']
        n = event['n']
        e = event['e']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            # 'type': 'chat_message',
            'content': 'chat_message',
            'message': message,
            'message2': message2,
            'logo': logo,
            'time': time.strftime("%H:%M:%S"),
            'n': n,
            'e': e,
        }))
