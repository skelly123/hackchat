from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from hashlib import md5


class Room(models.Model):
    title = models.CharField(max_length=255)
    permanent = models.BooleanField(default=0)

    def __str__(self):
        return self.title


class Room_Population(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    population = models.PositiveIntegerField(default=0)
    time = models.DateTimeField(default=now, blank=True)

    def __str__(self):
        return '{}:{}={}'.format(self.time, self.room, self.population)


class Private_Message(models.Model):
    from_a = models.ForeignKey(User, on_delete=models.CASCADE, related_name='from_a')
    to_b = models.ForeignKey(User, on_delete=models.CASCADE, related_name='to_b')
    message =  models.TextField(blank=False)
    time = models.DateTimeField(default=now, blank=True)
    read = models.BooleanField(default=0)

    def __str__(self):
        return '{}:{}={}@{}'.format(self.from_a, self.to_b, self.message, self.time)

class Private_Room(models.Model):
    link = models.CharField(max_length=255, unique=True)
    user_one = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_one')
    user_two = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_two')
    time = models.DateTimeField(default=now, blank=True)

    def save(self, **kwargs):
        bytes_string = bytes('{}{}'.format(self.user_one, self.user_two), 'utf-8')
        self.link = md5(bytes_string).hexdigest()
        super(Private_Room, self).save()

    def __str__(self):
        return '{}:{}@{}'.format(self.user_one, self.user_two, self.time)
