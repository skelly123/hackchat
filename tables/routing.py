from django.conf.urls import url
from . import consumers
# from channels.routing import route
# from example.consumers import ws_connect, ws_disconnect, ws_message

websocket_urlpatterns = [
    url(r'^ws/chat/(?P<room_name>[^/]+)/$', consumers.ChatConsumer),
    url(r'^ws/chat/$', consumers.RoomConsumer),
    url(r'^ws/inbox/$', consumers.InboxConsumer),
    url(r'^ws/private_chat/(?P<room_name>[^/]+)/$', consumers.PrivateChatConsumer),
]

# channel_routing = [
#     route('websocket.connect', ws_connect),
#     route('websocket.disconnect', ws_disconnect),
#     route("websocket.receive", ws_message),
# ]
