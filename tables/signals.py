from django.db.models.signals import post_save
from django.dispatch import receiver
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync



from .models import Room


# def send_message(event):
#     '''
#     Call back function to send message to the browser
#     '''
#     message = event['text']
#     channel_layer = channels.layers.get_channel_layer()
#     # Send message to WebSocket
#     async_to_sync(channel_layer.send)(text_data=json.dumps(
#         message
#     ))

# @receiver(post_save, sender=Room)
# async def room_save_handler(sender, instance, **kwargs):
#     print("########### SAVED!!! ###########")
#     channel_layer = get_channel_layer()
#
#     print(instance.__dict__)
#     await (channel_layer.group_send)(
#         'chat_room_dash',
#         {
#             'type': 'send_message',
#             'update': instance.population,
#         }
#     )
