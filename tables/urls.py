# chat/urls.py
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^inbox/$', views.inbox, name='inbox'),
    url(r'^(?P<room_name>[^/]+)/$', views.room, name='room'),
    url(r'^private/(?P<room_name>[^/]+)/$', views.private_room, name='private_room'),
    url(r'^ajax/key_gen/$', views.key_gen, name='key_gen'),
    url(r'^ajax/encrypt/$', views.encrypt, name='encrypt'),
    url(r'^ajax/decrypt/$', views.decrypt, name='decrypt'),
    url(r'^ajax/time_series/$', views.get_time_series, name='time_series'),
    url(r'^ajax/leader_board/$', views.get_leader_board, name='leader board'),

]
