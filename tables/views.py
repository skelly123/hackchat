from django.shortcuts import render
from django.utils.safestring import mark_safe
import json
from django.http import JsonResponse
from Crypto.PublicKey import RSA
from Crypto import Random
from .models import Room, Room_Population, Private_Message, Private_Room
from hashlib import md5
from django.http import HttpResponseNotFound
from django.contrib.auth.models import User



def index(request):
    return render(request, 'tables/index.html', {})


def room(request, room_name):
    return render(request, 'tables/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name)),
    })

def private_room(request, room_name):
    # hash = md5(b'{}{}'.format(request.user_one, request.user_two)).hexdigest()
    pr = Private_Room.objects.get(link=room_name)
    print("trying to access private room")
    other = [user for user in [pr.user_one, pr.user_two] if user!= request.user]

    if request.user in [pr.user_one, pr.user_two]:
        return render(request, 'tables/private_room.html', {
            'room_name_json': mark_safe(json.dumps(room_name)), 'partner': other[0].username,
        })
    else:
        return HttpResponseNotFound('<h1>No Page Here</h1>')

def inbox(request):
    username = None
    if request.user.is_authenticated:
        user = request.user
        messages = Private_Message.objects.filter(to_b=user).order_by('-time')
    return render(request, 'tables/inbox.html', {'messages':messages})



def key_gen(request):
    if request.method == 'POST':
        print(request)
        random_generator = Random.new().read
        key = RSA.generate(1024, random_generator)
        data = {
            'n': hex(key.n),
            'e': hex(key.e),
            'd': hex(key.d),
            'p': hex(key.p),
            'q': hex(key.q),
            'u': hex(key.u),
        }
        return JsonResponse(data)
    else:
        print("key_gen no work")
        return JsonResponse({})



def decrypt(request):
    encrypted_message = request.POST.get('encrypted_message', None)
    print(encrypted_message)
    encrypted_message = [int(i) for i in encrypted_message.split(',')]
    print(encrypted_message)
    n = int(request.POST.get('n', None), 16)
    e = int(request.POST.get('e', None), 16)
    d = int(request.POST.get('d', None), 16)
    p = int(request.POST.get('p', None), 16)
    q = int(request.POST.get('q', None), 16)
    u = int(request.POST.get('u', None), 16)
    priv_key = RSA.construct((n, e, d, p, q, u))
    print("#####")
    decrypted_message = priv_key.decrypt(bytes(encrypted_message))
    print("#######", decrypted_message, "#######")
    data = {
        'decrypted_message': str(decrypted_message, 'utf-8'),
    }
    return JsonResponse(data)


def encrypt(request):
    message = bytes(request.POST.get('message', None), 'utf-8')
    n = int(request.POST.get('n', None), 16)
    e = int(request.POST.get('e', None), 16)
    pub_key = RSA.construct((n, e))
    encrypted_message = pub_key.encrypt(message, 32)[0]
    encrypted_message = list(encrypted_message)
    data = {
        'encrypted_message': encrypted_message,
    }

    return JsonResponse(data)

def get_time_series(request):
    room_title = request.GET.get('label', None)
    print("#######",  request.GET, "#######")
    room = Room.objects.get(title=room_title)
    d2 = {}
    d2["room"]=room.title
    room_pop_data = Room_Population.objects.filter(room=room).order_by('time')
    d2["data"] = [{"x":item.time.timestamp(), "y":item.population} for item in room_pop_data]
    data = d2
    print(d2)
    return JsonResponse(data)

def get_leader_board(request):
    label_data_list = []
    for room in Room.objects.all():
        try:
            population = Room_Population.objects.filter(room=room).latest('time').population
        except:
            population = 0
        d = {}
        d["label"]=room.title
        d["data"]= population
        label_data_list.append(d)

    label_data_list = sorted(label_data_list, key=lambda k: k['data'], reverse=True)
    labels = [i['label'] for i in label_data_list]
    data = [i['data'] for i in label_data_list]
    d = {
        'labels': labels,
        'data': data,
        # 'time_series':room_pop_time,
        }
    return JsonResponse(d)
