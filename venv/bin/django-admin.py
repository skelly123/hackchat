#!/Users/sebastiankelly/dev/channels_sk/venv/bin/python3.7
from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
